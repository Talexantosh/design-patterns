<?php
declare(strict_types=1);

namespace Patterns\MVC;

if(isset($_GET['action']) && !empty($_GET['action']))
{
    $controller = new \Patterns\MVC\Controller\ConcreteController(); 
    die();
}  

$view = new \Patterns\MVC\View\ViewPage();

$model = new \Patterns\MVC\Model\ConcreteModel();
// Подписался на уведомления
$view->setModel($model);
$model->setView($view);

$controller = new \Patterns\MVC\Controller\Index($model);

echo $controller->execute();

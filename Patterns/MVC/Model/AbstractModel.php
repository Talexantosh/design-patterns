<?php
declare(strict_types=1);

namespace Patterns\MVC\Model;

abstract class AbstractModel{
    
    /**
     * Данные Модели
     * @var static string
     */
    protected static $data = 'MVC + PHP = нажмите здесь!';

     /**
      * Подписчик Вид
      * @var Patterns\MVC\View\AbstractView
      */
    private $view;

    abstract public function setData(string $data): void;

    abstract public function getData(): string;

    public function setView(\Patterns\MVC\View\ViewInterface $view)
    {
        $this->view = $view;
    }

    public function notify(): void
    {
        if (isset($this->view))
            $this->view->output();
    }
}
<?php
declare(strict_types=1);

namespace Patterns\MVC\Model;

class ConcreteModel extends AbstractModel
{
    public function setData(string $data): void
    {
        self::$data = $data;

        // Уведомим подписчика
        $this->notify();
    }

    public function getData(): string
    {
        return self::$data;
    }
}
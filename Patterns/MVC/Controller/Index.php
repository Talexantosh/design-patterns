<?php
declare(strict_types=1);

namespace Patterns\MVC\Controller;

class Index extends AbstractController
{
    private $method;

    public function execute()
    {
        if (isset($this->model))
            $this->model->notify();
    }
}

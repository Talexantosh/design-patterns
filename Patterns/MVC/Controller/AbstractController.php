<?php
declare(strict_types=1);

namespace Patterns\MVC\Controller;

abstract class AbstractController
{
    /**
     * Ссылка на модель, как субъект стратегии
     * @var \Patterns\MVC\Model\AbstractModel
     */
    protected $model;

    public function __construct(\Patterns\MVC\Model\AbstractModel $model = null){
        $this->model = $model;
    }

    public function setModel(\Patterns\MVC\Model\AbstractModel $model)
    {
        $this->model = $model;
    }

    abstract public function execute();
}
<?php
declare(strict_types=1);

namespace Patterns\MVC\Controller;

class ConcreteController extends AbstractController
{
    private $method;

    public function __construct(\Patterns\MVC\Model\AbstractModel $model = null)
    {
        parent::__construct($model);

        if(isset($_GET['action']) && !empty($_GET['action']))
        {
            $this->method = $_GET['action'];
            $this->{$this->method}();
        }    
    }

    public function execute()
    {
        // Выбор модели (стратегии)
        if (!isset($this->model))
        {
            $this->setModel(new \Patterns\MVC\Model\ConcreteModel());
            $view = new \Patterns\MVC\View\ViewPage();
            $this->model->setView($view);
            $view->setModel($this->model);
        }

        if (isset($this->model))
             $this->model->setData('Данные успешно записаны в модель, спасибо MVC!');
        else
            echo "Страница 404<br/>";
    }
}

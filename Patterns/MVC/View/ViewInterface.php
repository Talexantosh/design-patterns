<?php
declare(strict_types=1);

namespace Patterns\MVC\View;

/** 
 * Представление - это интерфейс компоновщика
*/
interface ViewInterface{
    public function output(): void;
    public function update(): string;
    public function setModel(\Patterns\MVC\Model\AbstractModel $model);
}

<?php
declare(strict_types=1);

namespace Patterns\MVC\View;


class ViewPage implements ViewInterface
{
    /**
     * @var Patterns\MVC\Model\AbstractModel
     */
    private $model;

    public function output(): void
    {
        echo '<p><a href="http://localhost/learn/Laravel/index.php?action=execute">' . $this->update() . "</a></p>";
    }

    public function update(): string
    {
        return $this->model->getData();
    }

    public function setModel(\Patterns\MVC\Model\AbstractModel $model)
    {
        $this->model = $model;
    }
}

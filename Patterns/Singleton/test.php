<?php
declare(strict_types=1);

    namespace Patterns\Singleton;

    use Patterns\Singleton\Database as Database;

    /**
     * Одиночка — это порождающий паттерн проектирования, 
     * который гарантирует, что у класса есть только один 
     * экземпляр, и предоставляет к нему глобальную точку 
     * доступа.
     */

    $database1 = Database::getInstance();
    $database1->query("SELECT1");

    $database2 = Database::getInstance();
    print_r ($database2->query("SELECT2"));

    try{
        $database = new Database();
    }
    catch(\Exception $e) {
        echo 'Выброшено исключение: ', $e->getMessage(), "\n";
    }


    
<?php
declare(strict_types=1);

    namespace Patterns\AbstractFactory;

    /**
     * Этот паттерн предполагает, что у вас есть несколько 
     * семейств продуктов, находящихся в отдельных иерархиях 
     * классов. Продукты одного семейства должны иметь
     * общий интерфейс.
     */
    abstract class EntityTwo{
        /**
         * Представление
         * @return void
         */
        public function getName():void{
            echo("<p>My Name is ".static::class."</p>");
        }

         /**
         * Сделать что-то полезное
         * Будем называть это общим интерфейсом
         * @return void
         */
        abstract public function doSomething():void;
    }
<?php
    namespace Patterns\AbstractFactory;

    /**
     * Абстрактная фабрика знает обо всех абстрактных типах
     * продуктов.
     */
    interface Factory{
        /**
         * Создать сущность
         * @return EntityOne
         */
        public function createEntityOne():\Patterns\AbstractFactory\EntityOne;

         /**
         * Создать сущность
         * @return EntityTwo
         */
        public function createEntityTwo():\Patterns\AbstractFactory\EntityTwo;
    }
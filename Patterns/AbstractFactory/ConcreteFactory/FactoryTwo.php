<?php
declare(strict_types=1);

    namespace Patterns\AbstractFactory\ConcreteFactory;

    use Patterns\AbstractFactory\Factory as Factory;
    use Patterns\AbstractFactory\ConcreteEntity\EntityOneFromFactoryTwo;
    use Patterns\AbstractFactory\ConcreteEntity\EntityTwoFromFactoryTwo;

    /**
     * Несмотря на то, что фабрики оперируют конкретными 
     * классами, их методы возвращают абстрактные типы 
     * продуктов. Благодаря этому фабрики можно взаимозаменять, 
     * не изменяя клиентский код.
     */
    class FactoryTwo implements Factory{
        /**
         * @inheritdoc
         */
        public function createEntityOne(){
            return new EntityOneFromFactoryTwo();
        }

         /**
         * @inheritdoc
         */
        public function createEntityTwo(){
            return new EntityTwoFromFactoryTwo();
        }
    }
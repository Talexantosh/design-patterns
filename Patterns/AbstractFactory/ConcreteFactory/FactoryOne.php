<?php
declare(strict_types=1);

    namespace Patterns\AbstractFactory\ConcreteFactory;

    use Patterns\AbstractFactory\Factory as Factory;
    use Patterns\AbstractFactory\ConcreteEntity\EntityOneFromFactoryOne;
    use Patterns\AbstractFactory\ConcreteEntity\EntityTwoFromFactoryOne;

    /**
     * Каждая конкретная фабрика знает и создаёт только
     * продукты своей вариации.
     */
    class FactoryOne implements Factory{
        /**
         * @inheritdoc
         */
        public function createEntityOne(){
            return new EntityOneFromFactoryOne();
        }

         /**
         * @inheritdoc
         */
        public function createEntityTwo(){
            return new EntityTwoFromFactoryOne();
        }
    }
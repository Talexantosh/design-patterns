<?php
declare(strict_types=1);

    namespace Patterns\AbstractFactory;

    use Patterns\AbstractFactory\Manager;
    use Patterns\AbstractFactory\Config\Order;

   /**
    * Приложение выбирает тип конкретной фабрики и создаёт её 
    * динамически, исходя из конфигурации или окружения.
    */
    
    $manager = new Manager();
    $order = new Order();
    $factory = $manager->getFactory($order->getIdFactory());
    $factory->createEntityOne()->doSomething();
    $factory->createEntityTwo()->doSomething();
    $order->setIdFactory(2);
    $factory = $manager->getFactory($order->getIdFactory());
    $factory->createEntityOne()->doSomething();
    $factory->createEntityTwo()->doSomething();
 /*   $order->setIdFactory(3);
    $factory = $manager->getFactory($order->getIdFactory());
    $factory->createEntityOne()->doSomething();
    $factory->createEntityTwo()->doSomething();
*/

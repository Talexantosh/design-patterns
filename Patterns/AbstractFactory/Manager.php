<?php
declare(strict_types=1);

    namespace Patterns\AbstractFactory;

    use Patterns\AbstractFactory\ConcreteFactory\FactoryOne;
    use Patterns\AbstractFactory\ConcreteFactory\FactoryTwo;

    /**
     * Менеджер
     */
    class Manager{

         /**
         * Отправить заказ на нужную фабрику
         * @param $idFactory
         * @return Factory
         * @throws \Exception
         */
        public function getFactory(int $idFactory):\Patterns\AbstractFactory\Factory{
            switch($idFactory){
                case 1:{
                    $factory = new FactoryOne();
                    break;
                }
                case 2:{
                    $factory = new FactoryTwo();
                    break;
                }
                default:{
                    throw new \Exception("We can not!");
                }
            }
            return $factory;
        }
    }
<?php
declare(strict_types=1);

    namespace Patterns\AbstractFactory\ConcreteEntity;

    use Patterns\AbstractFactory\EntityTwo as EntityTwo;

    /**
     * // Семейства продуктов имеют те же вариации
     */
    class EntityTwoFromFactoryTwo extends EntityTwo{

         /**
         * @inheritdoc
         */
        public function doSomething(){
            $this->getName();
        }
    }
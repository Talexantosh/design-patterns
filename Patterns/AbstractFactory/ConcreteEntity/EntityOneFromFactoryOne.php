<?php
declare(strict_types=1);

    namespace Patterns\AbstractFactory\ConcreteEntity;

    use Patterns\AbstractFactory\EntityOne as EntityOne;

    /**
     * // Семейства продуктов имеют те же вариации
     */
    class EntityOneFromFactoryOne extends EntityOne{

         /**
         * @inheritdoc
         */
        public function doSomething(){
            $this->getName();
        }
    }
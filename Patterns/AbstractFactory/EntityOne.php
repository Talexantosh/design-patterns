<?php
declare(strict_types=1);

    namespace Patterns\AbstractFactory;

    /**
     * Этот паттерн предполагает, что у вас есть несколько 
     * семейств продуктов, находящихся в отдельных иерархиях 
     * классов. Продукты одного семейства должны иметь
     * общий интерфейс.
     */
    abstract class EntityOne{
        /**
         * Представление
         */
        public function getName():void{
            echo("<p>My Name is ".static::class."</p>");
        }

         /**
         * Сделать что-то полезное
         */
        abstract public function doSomething():void;
    }
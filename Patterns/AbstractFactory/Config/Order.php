<?php
declare(strict_types=1);

    namespace Patterns\AbstractFactory\Config;

    /**
     * Хранит заказы
     */
    class Order{
        /**
         * Ид фабрики 
         * @var int
         */
        private int $idFactory=1;

        /**
         *  Геттер 
         * @return int
         */
        public function getIdFactory():int {
            return $this->idFactory;
        }

         /**
         * Сеттер
         * @param int
         */
        public function setIdFactory(int $idFactory):void {
            $this->idFactory = $idFactory;
        }
    }
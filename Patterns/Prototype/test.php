<?php
declare(strict_types=1);

    namespace Patterns\Prototype;

   use Patterns\Prototype\Shape\Circle;
   use Patterns\Prototype\Shape\Rectangle;

   $circle = new Circle(100, 105, 105, "red");
   $cloneCircle = $circle->duplicate();
   $cloneCircle->setX(55);
   $cloneCircle->setRadius(50);
   $circle->render();
   $cloneCircle->render();

   $rectangle = new Rectangle(50, 200, 50, 400, "yellow");
   $cloneRectangle = $rectangle->duplicate();
   $cloneRectangle->setX(300);
   $cloneRectangle->setColor("cyan");
   $rectangle->render();
   $cloneRectangle->render();
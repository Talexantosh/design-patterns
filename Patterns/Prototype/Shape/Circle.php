<?php
declare(strict_types=1);

    namespace Patterns\Prototype\Shape;

    use Patterns\Prototype\Shape as ShapeBase;

    /**
     * Все фигуры реализуют интерфейс клонирования и 
     * предоставляют метод для воспроизводства самой 
     * себя. Подклассы используют метод клонирования 
     * родителя, а затем копируют собственные поля в 
     * получившийся объект.
     */
    class Circle extends ShapeBase{
       
        /**
         * Радиус
         * @var int $radius
         */
        public int $radius;

        public function __construct(int $radius = 0, array ...$args)
        {
            $this->radius = $radius;

            parent::__construct(...$args);
        }

        /**
         * @inheritdoc
         */
        public function duplicate():Circle{
            return clone $this;
        }

        /**
         * @inheritdoc
         */
        public function render(){
            echo "<svg width=\"480\" height=\"368\">
                    <circle cx=\"$this->x\" cy=\"$this->y\" 
                    r=\"$this->radius\" stroke=\"black\" 
                    stroke-width=\"4\" fill=\"$this->color\" />
                  </svg>";
        }

    }
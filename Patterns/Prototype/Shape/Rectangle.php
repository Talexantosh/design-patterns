<?php
declare(strict_types=1);

    namespace Patterns\Prototype\Shape;

    use Patterns\Prototype\Shape as ShapeBase;

    /**
     * Все фигуры реализуют интерфейс клонирования и 
     * предоставляют метод для воспроизводства самой 
     * себя. Подклассы используют метод клонирования 
     * родителя, а затем копируют собственные поля в 
     * получившийся объект.
     */
    class Rectangle extends ShapeBase{
       
        /**
         * Ширина
         * @var int $width
         */
        public int $width;

        /**
         * Высота
         * @var int $height
         */
        public int $height;

        public function __construct(int $width = 0, int $height = 0, array ...$args)
        {
            $this->width = $width;
            $this->height = $height;

            parent::__construct(...$args);
        }

        /**
         * @inheritdoc
         */
        public function duplicate(){
            return clone $this;
        }

        /**
         * @inheritdoc
         */
        public function render(){
            echo "<svg width=\"480\" height=\"368\">
                    <rect width=\"$this->width\" height=\"$this->height\" 
                    style=\"fill:$this->color;stroke-width:3;stroke:rgb(0,0,0)\" />
                  </svg>";
        }

    }
<?php
declare(strict_types=1);

    namespace Patterns\Prototype;

    /**
     * Базовый прототип.
     */
    abstract class Shape{
        /**
        * @var int x
        */
        protected int $x;

        /**
        * @var int y
        */
        protected int $y;

        /**
        * @var string color
        */
        protected string $color;
        
        /**
         * Конструктор
         */
        public function __construct(int $x = 0, int $y = 0, string $color = "black"){
            $this->x = $x;
            $this->y = $y;
            $this->color = $color;
        }

        /**
         * Клонировать объект
         * Результатом операции клонирования всегда будет 
         * объект из иерархии классов Shape.
         * @return Shape duplicate
         */
        abstract public function duplicate():Shape;

        /**
         * Отобразить объект
         * @return void
         */
        abstract public function render():void;

        public function __call($method, $args){
            switch (substr($method, 0, 3)) {
                case 'set':{
                    $v = lcfirst(substr($method, 3));
                    $oldV = $this->$v;
                    $this->$v = $args[0];
                    return $oldV;
                }        
            }
        }
    }
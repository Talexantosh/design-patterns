<?php
declare (strict_types=1);

namespace Patterns\Bridge\Page;

use Patterns\Bridge\Page;
use Patterns\Bridge\Renderer;
use Patterns\Bridge\Product;


/**
 * Эта Конкретная Абстракция создаёт более сложную страницу.
 */
class ProductPage extends Page
{
    protected $product;

    public function __construct(Renderer $renderer, Product $product)
    {
        parent::__construct($renderer);
        $this->product = $product;
    }

    public function view(): string
    {
        return $this->renderer->renderParts([
            $this->renderer->renderHeader(),
            $this->renderer->renderTitle($this->product->getTitle()),
            $this->renderer->renderTextBlock($this->product->getDescription()),
            $this->renderer->renderImage($this->product->getImage()),
            $this->renderer->renderLink("/cart/add/" . $this->product->getId(), "Добавить в корзину"),
            $this->renderer->renderFooter()
        ]);
    }
}
<?php
declare (strict_types=1);

    namespace Patterns\Bridge;

    use Patterns\Bridge\Renderer\HTMLRenderer;
    use Patterns\Bridge\Renderer\JsonRenderer;
    use Patterns\Bridge\Page\ProductPage;
    use Patterns\Bridge\Page\SimplePage;
/**
 * В этом примере иерархия Страницы выступает как Абстракция, 
 * а иерархия Рендера как Реализация. Объекты класса Страница 
 * монтируют веб-страницы определённого типа, используя базовые 
 * элементы, которые предоставляются объектом Рендер, прикреплённым 
 * к этой странице. Обе иерархии классов разделены, поэтому можно 
 * добавить новый класс Рендер без изменения классов страниц и 
 * наоборот.
 */

/**
 * Клиентский код имеет дело только с объектами Абстракции.
 */
function clientCode(Page $page)
{
    // ...

    echo $page->view();

    // ...
}

/**
 * Клиентский код может выполняться с любой предварительно сконфигурированной
 * комбинацией Абстракция+Реализация.
 */
$HTMLRenderer = new HTMLRenderer();
$JSONRenderer = new JsonRenderer();

$page = new SimplePage($HTMLRenderer, "Home", "Добро пожаловать на наш сайт!");
echo "HTML-представление простой страницы с содержимым:\n";
clientCode($page);
echo "\n\n";

/**
 * При необходимости Абстракция может заменить связанную Реализацию во время
 * выполнения.
 */
$page->changeRenderer($JSONRenderer);
echo "JSON-представление простой страницы содержимого, отображаемой с помощью того же клиентского кода.:\n";
clientCode($page);
echo "\n\n";


$product = new Product("123", "Звездные войны, серия 1",
    "Давным-давно в далекой-далекой галактике ...",
    "/images/star-wars.jpeg", 39.95);

$page = new ProductPage($HTMLRenderer, $product);
echo "HTML-просмотр страницы продукта, тот же клиентский код:\n";
clientCode($page);
echo "\n\n";

$page->changeRenderer($JSONRenderer);
echo "Просмотр простой страницы содержимого в формате JSON с тем же клиентским кодом.:\n";
clientCode($page);
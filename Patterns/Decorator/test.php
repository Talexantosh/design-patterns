<?php
declare(strict_types=1);

namespace Patterns\Decorator;

/** В этом примере паттерн Декоратора помогает создать сложные правила 
 * фильтрации текста для приведения информации в порядок перед её 
 * отображением на веб-странице. Разные типы информации, такие как 
 * комментарии, сообщения на форуме или личные сообщения, требуют 
 * разных наборов фильтров.
 * 
 * Например, вы хотели бы удалить весь HTML из комментариев и в тоже 
 * время сохранить некоторые основные теги HTML в сообщениях на форуме. 
 * Кроме того, вы можете пожелать разрешить публикацию в формате Markdown, 
 * который должен быть обработан перед какой-либо фильтрацией HTML. Все 
 * эти правила фильтрации могут быть представлены в виде отдельных 
 * классов декораторов, которые могут быть сложены в стек по-разному, в 
 * зависимости от характера содержимого.
 */

/**
 * Клиентский код может быть частью реального веб-сайта, который отображает
 * создаваемый пользователями контент. Поскольку он работает с модулями
 * форматирования через интерфейс компонента, ему всё равно, получает ли он
 * простой объект компонента или обёрнутый.
 */
function displayCommentAsAWebsite(InputFormat $format, string $text)
{
    // ..

    echo $format->formatText($text);

    // ..
}

/**
 * Модули форматирования пользовательского ввода очень удобны при работе с
 * контентом, создаваемым пользователями. Отображение такого контента «как есть»
 * может быть очень опасным, особенно когда его могут создавать анонимные
 * пользователи (например, комментарии). Ваш сайт не только рискует получить
 * массу спам-ссылок, но также может быть подвергнут XSS-атакам.
 */
$dangerousComment = <<<HERE
Привет! Хороший пост в блоге!
Посетите его <a href='http://www.iwillhackyou.com'>homepage</a>.
<script src="http://www.iwillhackyou.com/script.js">
  performXSSAttack();
</script>
HERE;

/**
 * Наивное отображение комментариев (небезопасное).
 */
$naiveInput = new \Patterns\Decorator\InputFormat\TextInput();
echo "Сайт отображает комментарии без фильтрации (небезопасно):\n";
displayCommentAsAWebsite($naiveInput, $dangerousComment);
echo "\n\n\n";

/**
 * Отфильтрованное отображение комментариев (безопасное).
 */
$filteredInput = new \Patterns\Decorator\InputFormat\TextFormat\PlainTextFilter($naiveInput);
echo "Веб-сайт отображает комментарии после удаления всех тегов (безопасно):\n";
displayCommentAsAWebsite($filteredInput, $dangerousComment);
echo "\n\n\n";


/**
 * Декоратор позволяет складывать несколько входных форматов для получения
 * точного контроля над отображаемым содержимым.
 */
$dangerousForumPost = <<<HERE
# Добро пожаловать

Это мой первый пост на этом ** великолепном ** форуме.
<script src="http://www.iwillhackyou.com/script.js">
  performXSSAttack();
</script>
HERE;

/**
 * Наивное отображение сообщений (небезопасное, без форматирования).
 */
$naiveInput = new \Patterns\Decorator\InputFormat\TextInput();
echo "Website renders a forum post without filtering and formatting (unsafe, ugly):\n";
displayCommentAsAWebsite($naiveInput, $dangerousForumPost);
echo "\n\n\n";

/**
 * Форматтер Markdown + фильтрация опасных тегов (безопасно, красиво).
 */
$text = new \Patterns\Decorator\InputFormat\TextInput();
//$markdown = new \Patterns\Decorator\InputFormat\TextFormat\MarkdownFormat($text);
$filteredInput = new \Patterns\Decorator\InputFormat\TextFormat\DangerousHTMLTagsFilter($markdown);
echo "Веб-сайт отображает сообщение на форуме после перевода разметки ".
"и фильтрация некоторых опасных HTML-тегов и атрибутов (безопасно, красиво)s:\n";
displayCommentAsAWebsite($filteredInput, $dangerousForumPost);
echo "\n\n\n";
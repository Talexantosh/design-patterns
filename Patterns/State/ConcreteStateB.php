<?php

namespace Patterns\State;

class ConcreteStateB extends State
{
    public function handle1(): void
    {
        echo "ConcreteStateB обрабатывает запрос 1.<br/>";
    }

    public function handle2(): void
    {
        echo "ConcreteStateB обрабатывает запрос 2.<br/>";
        echo "ConcreteStateB хочет изменить состояние контекста.<br/>";
        $this->context->setState(new ConcreteStateA());
    }
}

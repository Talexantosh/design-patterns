<?php

namespace Patterns\State;

/**
 * Клиентский код.
 */
$context = new Context(new ConcreteStateA());
$context->doHandle1();
$context->doHandle2();
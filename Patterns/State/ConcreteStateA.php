<?php

namespace Patterns\State;

/**
 * Конкретные Состояния реализуют различные модели поведения, связанные с
 * состоянием Контекста.
 */
class ConcreteStateA extends State
{
    public function handle1(): void
    {
        echo "ConcreteStateA обрабатывает запрос 1.<br/>";
        echo "ConcreteStateA хочет изменить состояние контекста.<br/>";
        $this->context->setState(new ConcreteStateB());
    }

    public function handle2(): void
    {
        echo "ConcreteStateA обрабатывает запрос 2.<br/>";
    }
}

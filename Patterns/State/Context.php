<?php

namespace Patterns\State;

/**
 * Контекст определяет интерфейс, представляющий интерес для клиентов. Он также
 * хранит ссылку на экземпляр подкласса Состояния, который отображает текущее
 * состояние Контекста.
 */
class Context
{
    /**
     * @var State Ссылка на текущее состояние Контекста.
     */
    private $state;

    public function __construct(State $state)
    {
        $this->setState($state);
    }

    /**
     * Контекст позволяет изменять объект Состояния во время выполнения.
     */
    public function setState(State $state): void
    {
        echo "Контекст: переходит к " . get_class($state) . ".<br/>";
        $this->state = $state;
        $this->state->setContext($this);
    }

    /**
     * Контекст делегирует часть своего поведения текущему объекту Состояния.
     */
    public function doHandle1(): void
    {
        $this->state->handle1();
    }

    public function doHandle2(): void
    {
        $this->state->handle2();
    }
}

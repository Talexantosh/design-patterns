<?php

namespace Patterns\TemplateMethod;

class ConcreteClass2 extends AbstractClass
{
    protected function requiredStep4(): void
    {
        echo "ConcreteClass2: Реализует шаг номер 4<br/>";
    }

    protected function requiredStep5(): void
    {
        echo "ConcreteClass2: Реализует шаг номер 5<br/>";
    }
}
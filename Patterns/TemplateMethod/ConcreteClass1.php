<?php

namespace Patterns\TemplateMethod;

/**
 * Конкретные классы должны реализовать все абстрактные операции базового
 * класса. Они также могут переопределить некоторые операции с реализацией по
 * умолчанию.
 */
class ConcreteClass1 extends AbstractClass
{
    protected function requiredStep4(): void
    {
        echo "ConcreteClass1: Реализует шаг номер 4<br/>";
    }

    protected function requiredStep5(): void
    {
        echo "ConcreteClass1: Реализует шаг номер 5<br/>";
    }
}

<?php

namespace Patterns\TemplateMethod;

/**
 * Клиентский код вызывает шаблонный метод для выполнения алгоритма. Клиентский
 * код не должен знать конкретный класс объекта, с которым работает, при
 * условии, что он работает с объектами через интерфейс их базового класса.
 */
function clientCode(AbstractClass $class)
{
    // ...
    $class->templateMethod();
    // ...
}

echo "Один и тот же клиентский код может работать с разными подклассами: <br/>";
clientCode(new ConcreteClass1());
echo "\n";

echo "Один и тот же клиентский код может работать с разными подклассами: <br/>";
clientCode(new ConcreteClass2());
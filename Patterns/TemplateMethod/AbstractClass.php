<?php

namespace Patterns\TemplateMethod;

/**
 * Абстрактный Класс определяет шаблонный метод
 *
 * Конкретные подклассы должны реализовать операции, но оставить сам
 * шаблонный метод без изменений.
 */
abstract class AbstractClass
{
    /**
     * Шаблонный метод.
     */
    final public function templateMethod(): void
    {
        $this->baseStep1();
        $this->baseStep2();
        $this->requiredStep4();
        $this->requiredStep5();
        $this->baseStep3();
    }

    /**
     * Эти операции имеют реализации.
     */
    protected function baseStep1(): void
    {
        echo "AbstractClass: Сделаeт основную часть работы<br/>";
    }

    protected function baseStep2(): void
    {
        echo "AbstractClass: подклассы доделают все остальное<br/>n";
    }

    protected function baseStep3(): void
    {
        echo "Абстрактный класс все равно делает основную работу<br/>";
    }

    /**
     * А эти операции должны быть реализованы в подклассах.
     */
    abstract protected function requiredStep4(): void;

    abstract protected function requiredStep5(): void;

}

<?php
declare(strict_types=1);

namespace Patterns\Observer;

use Patterns\Observer\EventListener;

// Класс-издатель. Содержит код управления подписчиками
// и их оповещения.
class EventManager {

    /**
     * @var EventListener[]
     */
    private array $listeners = [];

    /**
     * @param EventListener
     * @return void
     */
    public function subscribe(EventListener $listener):void {
        $this->listeners[] = $listener;
    }
    
    /**
     * @param EventListener
     * @return void
     */
    public function unsubscribe(EventListener $listener):void{
        if (($key = array_search($listener, $this->listeners)) !== false) {
            unset($this->listeners[$key]);
        }
    }
         

    public function notify(string $data) {
        foreach ($this->listener as $listener) {
            $listener->update("ReadMe.md");
        }
    }
}
        
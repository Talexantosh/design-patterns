<?php
declare(strict_types=1);

namespace Patterns\Observer;

// Общий интерфейс подписчиков. 
interface EventListener {

    /**
     * @param string
     * @return void
     */
    public function update(string $filename):void;
}

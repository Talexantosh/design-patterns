<?php
declare(strict_types=1);

namespace Patterns\Observer;

// Конкретный класс-издатель.
class Editor {

    /**
     * @var EventManager
     */
    public EventManager  $events; 

    // Методы бизнес-логики, которые оповещают подписчиков об
    // изменениях.
    
    public function saveFile():void {
        $this->events->notify("Important.doc");
    // ...
    }
}

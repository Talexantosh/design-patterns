<?php
declare(strict_types=1);

namespace Patterns\Observer;

use Patterns\Observer\EventListener\EmailAlertsListener;
use Patterns\Observer\EventListener\LoggingListener;
use Patterns\Observer\Editor;
use Patterns\Observer\EventManager;

/**
 * Наблюдатель — это поведенческий паттерн проектирования, 
 * который создаёт механизм подписки, позволяющий одним 
 * объектам следить и реагировать на события, происходящие 
 * в других объектах.
 */

$eventManager = new EventManager();
$eventManager->subscribe(new EmailAlertsListener());
$eventManager->subscribe(new LoggingListener());

$editor = new Editor();
$editor->events = $eventManager;
$editor->saveFile();

/**
 * Когда после изменения состояния одного объекта требуется что-то 
 * сделать в других, но вы не знаете наперёд, какие именно объекты 
 * должны отреагировать.
 * Когда одни объекты должны наблюдать за другими, но только в 
 * определённых случаях.
 */
<?php
declare(strict_types=1);

namespace Patterns\Observer\EventListener;

use Patterns\Observer\EventListener;


class LoggingListener implements EventListener {

    /**
     * @var string
     */
    private string $log = '';
  
    /**
     * @inheritdoc
     */
    public function update($filename) {
       $this->log .=  $filename;
    }
}
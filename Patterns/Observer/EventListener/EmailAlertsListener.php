<?php
declare(strict_types=1);

namespace Patterns\Observer\EventListener;

use Patterns\Observer\EventListener;


class EmailAlertsListener implements EventListener {

    /**
     * @inheritdoc
     */
    public function update($filename) {
       mail("Head", "head@example.com", $filename);
    }
}
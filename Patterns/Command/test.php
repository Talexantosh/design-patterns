<?php
declare(strict_types=1);

    namespace Patterns\Command;

    use Patterns\Command\Editor;

    /**
     * Команда — это поведенческий паттерн проектирования, 
     * который превращает запросы в объекты, позволяя 
     * передавать их как аргументы при вызове методов, 
     * ставить запросы в очередь, логировать их, а также 
     * поддерживать отмену операций.
     */
    $editor = new Editor();
    $copy = new \Patterns\Command\Command\Copy($editor);
    $btnCopy = new  \Patterns\Command\UIComponent\ButtonCopy($copy);
    $shcCopy = new \Patterns\Command\UIComponent\ShortcutCopy($copy);

    $btnCopy->onCopy();

    /**
     * Когда вы хотите параметризовать объекты выполняемым 
     * действием.
     * Когда вы хотите ставить операции в очередь, выполнять 
     * их по расписанию или передавать по сети.
     * Когда вам нужна операция отмены.
     */
    
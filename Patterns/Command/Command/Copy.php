<?php
declare(strict_types=1);

    namespace Patterns\Command\Command;

    use Patterns\Command\AbstractCommand;

    /**
     *  Конкретная команда.
     */ 
    class Copy extends AbstractCommand {
        /**
         * Команда копирования не записывается в историю, 
         * так как она не меняет состояние редактора.
         * @return bool
         */
        public function execute():bool {
            $this->backup = $this->editor->getSelection();
            return false;
        }
    }

    
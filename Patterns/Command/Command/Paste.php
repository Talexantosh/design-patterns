<?php
declare(strict_types=1);

    namespace Patterns\Command\Command;

    use Patterns\Command\AbstractCommand;

    /**
     *  Конкретная команда.
     */ 
    class Paste extends AbstractCommand {
        /**
         * @return bool
         */
        public function execute():bool {
            $this->editor->replaceSelection($this->backup);
            return true;
        }
    }

    
<?php
declare(strict_types=1);

    namespace Patterns\Command\UIComponent;

    /**
     * UI Component
     */

     class ButtonCopy {
        
        /**
         * @var Copy
         */
        private \Patterns\Command\Command\Copy $command;
        /**
         * constructor
         * @param $commandHandler
         */
        public function __construct(\Patterns\Command\AbstractCommand $command){
            $this->command = $command;
        }

        public function onCopy():void{
            $this->command->execute();
        }
     }

    
<?php
    namespace Patterns\Command\UIComponent;

    /**
     * UI Component
     */

     class ShortcutCopy {
        
        /**
         * @var Copy
         */
        private \Patterns\Command\Command\Copy $command;
        /**
         * constructor
         * @param $commandHandler
         */
        public function __construct(\Patterns\Command\AbstractCommand $command){
            $this->command = $command;
        }

        public function onCopy():void{
            $this->command->execute();
        }
     }

    
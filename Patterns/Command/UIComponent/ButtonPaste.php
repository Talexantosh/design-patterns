<?php
declare(strict_types=1);

    namespace Patterns\Command\UIComponent;

    /**
     * UI Component
     */

     class ButtonPaste {
        
        /**
         * @var Paste
         */
        private \Patterns\Command\Command\Paste $command;
        /**
         * constructor
         * @param $commandHandler
         */
        public function __construct(\Patterns\Command\AbstractCommand $command){
            $this->command = $command;
        }

        public function onPaste():void{
            $this->command->execute();
        }
     }

    
<?php
declare(strict_types=1);

    namespace Patterns\Command;

    use Patterns\Command\Application;
    use Patterns\Command\Editor;

    /**
     * Абстрактная команда задаёт общий интерфейс для 
     * конкретных классов команд и содержит базовое 
     * поведение отмены операции.
     */

     abstract class AbstractCommand {

        /**
         * @var Editor
         */
        protected Editor $editor;

        /**
         * @var string
         */
        protected string $backup;

        /**
         * constructor
         */
        public function __construct(Editor $editor){
            $this->editor = $editor;
        }
    
        /**
         * Главный метод команды остаётся абстрактным, 
         * чтобы каждая конкретная команда определила 
         * его по-своему. Метод должен возвратить true 
         * или false в зависимости о того, изменила ли 
         * команда состояние редактора, а значит, нужно 
         * ли её сохранить в истории.
        */
        abstract public function execute():void;
    
     }

    
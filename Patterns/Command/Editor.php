<?php
declare(strict_types=1);

    namespace Patterns\Command;

    /** Класс редактора содержит непосредственные операции 
     * над текстом. Он отыгрывает роль получателя — команды 
     * делегируют ему свои действия.
     */
    class Editor {
        /**
         * @var string
         */
        private string $text;

        /**
         * Вернуть выбранный текст.
         * @todo not implemented
         */
        public function getSelection():string {
            $selexted = "selected text";
            // TODO
            return $selexted;
        } 

        /**
         * Вставить текст из буфера обмена в текущей позиции.
         * @param string
         * @todo not implemented
         */
        public function replaceSelection(string $text):void {

            $this->text = $text;
        }   
    }

    
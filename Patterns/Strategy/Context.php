<?php
declare(strict_types=1);

namespace Patterns\Strategy;

/**
 * Контекст определяет интерфейс, представляющий интерес для клиентов.
 */
class Context
{
    /**
     * Он должен работать со
     * всеми стратегиями через интерфейс Стратегии.
     * @var Strategy Контекст хранит ссылку на один из объектов Стратегии.
     */
    private Strategy $strategy;

    /**
     * Контекст принимает стратегию через конструктор, а также
     * предоставляет сеттер для её изменения во время выполнения.
     */
    public function __construct(Strategy $strategy)
    {
        $this->strategy = $strategy;
    }

    /**
     * Контекст позволяет заменить объект Стратегии во время выполнения.
     */
    public function setStrategy(Strategy $strategy)
    {
        $this->strategy = $strategy;
    }

    /**
     * Вместо того, чтобы самостоятельно реализовывать множественные версии
     * алгоритма, Контекст делегирует работу объекту Стратегии.
     */
    public function execute(array $data): void
    {
        // ...

        echo "Контекст: сортировка данных.<br/>";
        $result = $this->strategy->sortStrategy($data);
        echo implode(",", $result) . "<br/>";

        // ...
    }
}

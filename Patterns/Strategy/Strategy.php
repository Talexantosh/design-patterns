<?php
declare(strict_types=1);

namespace Patterns\Strategy;

/**
 * Контекст использует этот интерфейс для вызова алгоритма, определённого
 * Конкретными Стратегиями.
 */
interface Strategy
{
    public function sortStrategy(array $data): array;
}

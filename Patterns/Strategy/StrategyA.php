<?php
declare(strict_types=1);

namespace Patterns\Strategy;

/**
 * Конкретные Стратегии реализуют алгоритм, следуя базовому интерфейсу
 * Стратегии. Этот интерфейс делает их взаимозаменяемыми в Контексте.
 */
class StrategyA implements Strategy
{
    public function sortStrategy(array $data): array
    {
        sort($data);

        return $data;
    }
}

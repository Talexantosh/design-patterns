<?php
declare(strict_types=1);

namespace Patterns\Strategy;

class StrategyB implements Strategy
{
    public function sortStrategy(array $data): array
    {
        rsort($data);

        return $data;
    }
}

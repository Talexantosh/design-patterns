<?php
declare(strict_types=1);

namespace Patterns\Strategy;

/**
 * Клиентский код выбирает конкретную стратегию и передаёт её в контекст. Клиент
 * должен знать о различиях между стратегиями, чтобы сделать правильный выбор.
 */
$data = ['a', 'f', 'd', 'e', 'b', 'g', 'c'];
echo 'Данные: <br/>';
echo implode(",", $data) . "<br/><br/>";

$context = new Context(new StrategyA());
echo "Клиент: для стратегии выбрана обычная сортировка.<br/>";
$context->execute($data);

echo "<br/>";

echo "Клиент: стратегия настроена на обратную сортировку.<br/>";
$context->setStrategy(new StrategyB());
$context->execute($data);
<?php
declare(strict_types=1);

namespace Patterns\Memento;

use Patterns\Memento\Editor;
use Patterns\Memento\Snapshot;

// Опекуном может выступать класс команд (см. паттерн Команда).
// В этом случае команда сохраняет снимок состояния объекта-
// получателя, перед тем как передать ему своё действие. А в
// случае отмены команда вернёт объект в прежнее состояние.
class Command {

    /**
     * @var Snapshot[]
     */
    private array $backup = [];

    /**
     * @var Editor
     */
    private Editor $editor;

    public function save():void {
        $this->backup[] = $this->editor->makeSnapshot();
    }

    public function undo():void {
        $this->editor->restore(array_pop($this->backup));
    }

    public function __construct(Editor $editor)
    {
        $this->editor = $editor;
    }

    // ...
}
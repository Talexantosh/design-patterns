<?php
declare(strict_types=1);

namespace Patterns\Memento;

use Patterns\Memento\Editor;

// Снимок хранит прошлое состояние редактора.
class Snapshot {

    /**
     * @var array
     */
    private array $state;

    public function __construct(array $state = []) {
        foreach ($state as $k => $v) {
            $this->state[$k] = clone $v;
        }
    }

    // В нужный момент владелец снимка может восстановить
    // состояние редактора.
    public function getState():array {
       return $this->state;
    }
}
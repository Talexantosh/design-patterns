<?php
declare(strict_types=1);

namespace Patterns\Memento;

use Patterns\Memento\Snapshot;

// Класс создателя должен иметь специальный метод, который
// сохраняет состояние создателя в новом объекте-снимке.
class Editor {
    /**
     * @var array
     */
    private $state = [];

    /**
     * @return Snapshot
     */
    public function makeSnapshot():Snapshot{
        // Снимок — неизменяемый объект, поэтому Создатель
        // передаёт все своё состояние через параметры
        // конструктора.
        return new Snapshot($this->state);
    }

    /**
     * @param Snapshot
     * @return void
     */
    public function restore(Snapshot $snapshot):void{
        
        foreach ($snapshot->getState() as $k => $v) {
            $this->state[$k] = clone $v;
        }
    }
}
<?php
declare(strict_types=1);

namespace Patterns\Facade;

/**
 * Подсистема может принимать запросы либо от фасада, либо от клиента напрямую.
 * В любом случае, для Подсистемы Фасад – это еще один клиент, и он не является
 * частью Подсистемы.
 */
class Subsystem1
{
    public function operation1(): string
    {
        return "Подсистема1: Готова!\n";
    }

    // ...

    public function operationN(): string
    {
        return "Подсистема1: Вперед!\n";
    }
}

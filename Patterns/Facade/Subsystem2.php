<?php
declare(strict_types=1);

namespace Patterns\Facade;


/**
 * Некоторые фасады могут работать с разными подсистемами одновременно.
 */
class Subsystem2
{
    public function operation1(): string
    {
        return "Подсистема2: Готовься!\n";
    }

    // ...

    public function operationZ(): string
    {
        return "Подсистема2: Огонь!\n";
    }
}

<?php

namespace Patterns\Adapter;

use Patterns\Adapter\Notification;
use Patterns\Adapter\Notification\EmailNotification;
use Patterns\Adapter\Notification\SlackNotification;

/**
 * Паттерн Адаптер позволяет использовать сторонние или устаревшие классы, 
 * даже если они несовместимы с основной частью кода. Например, вместо того, 
 * чтобы переписывать интерфейс уведомлений вашего приложения для поддержки 
 * каждого стороннего сервиса вроде Slack, Facebook, SMS и прочих, вы 
 * создаёте под эти сервисы набор специальных обёрток, которые приводят 
 * вызовы из приложения к требуемым сторонними классами интерфейсу и формату.
 */ 
 
///////////////////////////////////////////////////////////////////////////
/**
 * Клиентский код.
 */
function clientCode(Notification $notification)
{
    // ...

    echo $notification->send("Сайт не работает!",
        "<strong style='color:red;font-size: 50px;'>Alert!</strong> " .
        "Наш сайт не отвечает. Звоните админам и поднимайте вопрос!");

    // ...
}

echo "Код клиента разработан правильно и работает с уведомлениями по электронной почте.:\n";
$notification = new EmailNotification("developers@example.com");
clientCode($notification);
echo "\n\n";


echo "Тот же клиентский код может работать с другими классами через адаптер:\n";
$slackApi = new SlackApi("example.com", "XXXXXXXX");
$notification = new SlackNotification($slackApi, "Example.com Developers");
clientCode($notification);
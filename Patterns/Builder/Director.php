<?php
declare(strict_types=1);

    namespace Patterns\Builder;

    use Patterns\Builder\BuilderInterface;

    /**
     * Директор определяет порядок вызова строительных шагов 
     * для производства той или иной конфигурации продуктов.
     */
    class Director{
        /**
        * Стройбригада:)
        * @var BuilderInterface
        */
        private BuilderInterface $builder;

        /**
         * Конструктор
         * @param BuildInterface|null
         */
        public function __construct(BuilderInterface $builder = null)
        {
            $this->builder = $builder;
        }

        /**
         * Текучесть кадров
         * @param BuilderInterface 
         * @return void
         */
        public function changeBuilder(BuilderInterface $builder):void{
            $this->builder = $builder;
        }

        /**
         * Вылатить премию
         * @param string
         * @return BuilderInterface
         */
        public function make(string $type): BuilderInterface{
            $this->builder->reset();
            switch($type){
                case 'one':{
                    $this->builder->buildStepA();
                    break;
                }
                case 'two':{
                    $this->builder->buildStepB();
                    $this->builder->buildStepZ();
                    break;
                }
                default:{
                    throw new \Exception("We do not know how!");
                }
            }

            return $this->builder;
        }
    }
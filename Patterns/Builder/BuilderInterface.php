<?php
declare(strict_types=1);

    namespace Patterns\Builder;
    use \Patterns\Builder\ProductOne;
    use \Patterns\Builder\ProductTwo;

    /**
     * // Строитель может создавать различные продукты, 
     * используя один и тот же процесс строительства.
     */
    interface BuilderInterface{
        /**
         * Интерфейс строителя объявляет шаги конструирования 
         * продуктов, общие для всех видов строителей
         */
        /**
         * Начать строительство
         * @return void
         */
        public function reset():void;

        /**
         * Выполнить шаг А
         * @return void
         */
        public function buildStepA():void;

        /**
         * Выполнить шаг B
         * @return void
         */
        public function buildStepB():void;

         /**
         * Выполнить шаг Z
         * @return void
         */
        public function buildStepZ():void;

        /**
         * Получить конечный продукт
         * @return Object
         */
        public function getProduct(): Object;
    }
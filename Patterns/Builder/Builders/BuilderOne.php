<?php
declare(strict_types=1);

    namespace Patterns\Builder\Builders;

    use Patterns\Builder\BuilderInterface;
    use Patterns\Builder\ProductOne;

    /**
     * Конкретные строители реализуют строительные шаги, 
     * каждый по-своему. Конкретные строители могут производить
     * разнородные объекты, не имеющие общего интерфейса.
     */
    class BuilderOne implements BuilderInterface{
        /**
         * В отличие от других порождающих паттернов, где 
         * продукты должны быть частью одной иерархии классов
         * или следовать общему интерфейсу, строители могут
         * создавать совершенно разные продукты, которые не 
         * имеют общего предка.
         */

         /**
          * Конечный продукт
          * @var Product
          */
          private \Patterns\Builder\ProductOne $product;

        /**
         * @inheritdoc
         */
        public function reset(){
            $this->product = new ProductOne();
        }

        /**
         * @inheritdoc
         */
        public function buildStepA(){
            $this->product->setFeatureA();
        }

        /**
         * @inheritdoc
         */
        public function buildStepB(){
            $this->product->setFeatureB();
        }

         /**
         * @inheritdoc
         */
        public function buildStepZ(){
            $this->product->setFeatureZ();
        }

        /**
         * @inheritdoc
         */
        public function getProduct(){
            return $this->product;
        }
    }
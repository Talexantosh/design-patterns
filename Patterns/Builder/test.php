<?php
declare(strict_types=1);

    namespace Patterns\Builder;

    use Patterns\Builder\Director;
    use Patterns\Builder\Builders\BuilderOne;
    use Patterns\Builder\Builders\BuilderTwo;

   /**
    * Строитель — это порождающий паттерн проектирования, 
    * который позволяет создавать сложные объекты пошагово. 
    * Строитель даёт возможность использовать один и тот же 
    * код строительства для получения разных представлений 
    * объектов.
    */
    
    $director = new Director(new BuilderOne());
    $director->make("two")->getProduct()->displayProperties();
    $director->make("one")->getProduct()->displayProperties();
    
    $director = new Director(new BuilderTwo());
    $director->make("two")->getProduct()->displayProperties();
    $director->make("one")->getProduct()->displayProperties();

    /**
     * Применяемость:
     * Когда вы хотите избавиться от «телескопического 
     * конструктора».
     * Когда ваш код должен создавать разные представления 
     * какого-то объекта. Например, деревянные и железобетонные 
     * дома.
     * Когда вам нужно собирать сложные составные объекты, 
     * например, деревья Компоновщика. 
     */

     /**
      * Преимущества и недостатки:
      * Позволяет создавать продукты пошагово.
      * Позволяет использовать один и тот же код для создания 
      * различных продуктов.
      * Изолирует сложный код сборки продукта от его основной 
      * бизнес-логики.
      * Усложняет код программы из-за введения дополнительных 
      * классов.
      * Клиент будет привязан к конкретным классам строителей, 
      * так как в интерфейсе директора может не быть метода 
      * получения результата.
      */
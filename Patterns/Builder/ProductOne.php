<?php
declare(strict_types=1);

    namespace Patterns\Builder;

    /**
     * Продукт — создаваемый объект. Продукты, сделанные разными
     * строителями, не обязаны иметь общий интерфейс.
     */
    class ProductOne{
        /**
        * Свойства продукта
        * @var array
        */
        private array $property = [];

        /**
         * Сеттер свойств
         * @inheritdoc
         */
        public function __call($name, $arguments)
        {
            switch (substr($name, 0, 3)) {
                case 'set':{
                    $this->property[] = lcfirst(substr($name, 3));
                    return;
                }        
            }
        }

        /**
         * Показать свойства продукта
         * @return void
         */
        public function displayProperties():void{
            echo '<p>Product One. Property: '.
            print_r($this->property, true).'</p>';
        }
    }
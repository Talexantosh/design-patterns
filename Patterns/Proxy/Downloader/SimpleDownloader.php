<?php
declare(strict_types=1);

namespace Patterns\Proxy\Downloader;
use Patterns\Proxy\Downloader;


/**
 * Реальный Субъект делает реальную работу, хотя и не самым эффективным
 * способом. Когда клиент пытается загрузить тот же самый файл во второй раз,
 * наш загрузчик именно это и делает, вместо того, чтобы извлечь результат из
 * кэша.
 */
class SimpleDownloader implements Downloader
{
    public function download(string $url): string
    {
        echo "Скачивание файла из Интернета.\n";
        $result = file_get_contents($url);
        echo "Скачанные байты: " . strlen($result) . "\n";

        return $result;
    }
}

<?php
declare(strict_types=1);

namespace Patterns\CoR;

// Интерфейс обработчиков.
interface ComponentWithContextualHelp {

    
    public function showHelp():bool;
}

<?php
declare(strict_types=1);

namespace Patterns\CoR\Component;

use Patterns\CoR\ComponentWithContextualHelp;

class Dialog implements ComponentWithContextualHelp {
    
    /**
     * @var ComponentWithContextualHelp
     */
    private ComponentWithContextualHelp $parent;

    /**
     * @var string
     */
    private stRing $helpText = 'The dialog have context help!';


    public function showHelp():bool {
        if (!strlen($this->helpText)){
            echo $this->helpText;
            return true;
        }
        else
            return $this->parent->showHelp();
    }

    /**
     * @param ComponentWithContextualHelp
     */
    public function __construct(ComponentWithContextualHelp $parent = null){
        $this->parent = $parent;
    }
}


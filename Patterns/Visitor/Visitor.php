<?php

namespace Patterns\Visitor;

/**
 * Сигнатура метода посещения позволяет посетителю
 * определить конкретный класс компонента, с которым он имеет дело.
 */
interface Visitor
{
    public function visitComponentA(ComponentA $element): void;

    public function visitComponentB(ComponentB $element): void;
}

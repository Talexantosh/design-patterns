<?php

namespace Patterns\Visitor;

class Visitor2 implements Visitor
{
    public function visitComponentA(ComponentA $element): void
    {
        echo $element->someMethod() . " + Visitor2<br/>";
    }

    public function visitComponentB(ComponentB $element): void
    {
        echo $element->getName() . " + Visitor2<br/>";
    }
}

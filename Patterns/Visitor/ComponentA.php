<?php

namespace Patterns\Visitor;

/**
 * Каждый Конкретный Компонент должен реализовать метод accept таким образом,
 * чтобы он вызывал метод посетителя, соответствующий классу компонента.
 */
class ComponentA implements Component
{
    /**
     * Мы вызываем visitComponentA. Таким образом мы позволяем посетителю узнать, с
     * каким классом компонента он работает.
     */
    public function accept(Visitor $visitor): void
    {
        $visitor->visitComponentA($this);
    }

    
    public function someMethod(): string
    {
        return "A";
    }
}

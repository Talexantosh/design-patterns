<?php

namespace Patterns\Visitor;

class ComponentB implements Component
{
    public function accept(Visitor $visitor): void
    {
        $visitor->visitComponentB($this);
    }

    public function getName(): string
    {
        return "B";
    }
}

<?php

namespace Patterns\Visitor;

/**
 * Клиентский код может выполнять операции посетителя над любым набором
 * элементов, не выясняя их конкретных классов. 
 */
function clientCode(array $components, Visitor $visitor)
{
    // ...
    foreach ($components as $component) {
        $component->accept($visitor);
    }
    // ...
}

$components = [
    new ComponentA(),
    new ComponentB(),
];

echo "Клиентский код работает со всеми посетителями через базовый интерфейс посетителей:<br/>";
$visitor1 = new Visitor1();
clientCode($components, $visitor1);
echo "<br/>";

echo "Это позволяет одному и тому же клиентскому коду работать с разными типами посетителей:<br/>";
$visitor2 = new Visitor2();
clientCode($components, $visitor2);
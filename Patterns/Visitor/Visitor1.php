<?php

namespace Patterns\Visitor;

/**
 * Конкретные Посетители реализуют несколько версий одного и того же алгоритма,
 * которые могут работать со всеми классами конкретных компонентов.
 *
 * Максимальную выгоду от паттерна Посетитель вы почувствуете, используя его со
 * сложной структурой объектов, такой как дерево Компоновщика.
 */
class Visitor1 implements Visitor
{
    public function visitComponentA(ComponentA $element): void
    {
        echo $element->someMethod() . " + Visitor1<br/>";
    }

    public function visitComponentB(ComponentB $element): void
    {
        echo $element->getName() . " + Visitor1<br/>";
    }
}

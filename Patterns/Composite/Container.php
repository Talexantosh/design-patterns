<?php

namespace Patterns\Composite;

/**
 * Класс Контейнер содержит сложные компоненты, которые могут иметь вложенные
 * компоненты.
 */
class Container extends Component
{
    /**
     * @var \SplObjectStorage
     */
    protected \SplObjectStorage $children;

    public function __construct()
    {
        $this->children = new \SplObjectStorage();
    }

    public function addChild(Component $component): void
    {
        $this->children->attach($component);
        $component->setParent($this);
    }

    public function removeChild(Component $component): void
    {
        $this->children->detach($component);
        $component->setParent(null);
    }

    public function isComposite(): bool
    {
        return true;
    }

    /**
     * Контейнер выполняет свою основную логику особым образом. Он проходит
     * рекурсивно через всех своих детей, собирая и суммируя их результаты.
     * Поскольку потомки контейнера передают эти вызовы своим потомкам и так
     * далее, в результате обходится всё дерево объектов.
     */
    public function operation(): string
    {
        $results = [];
        foreach ($this->children as $child) {
            $results[] = $child->operation();
        }

        return "Ветвь(" . implode("+", $results) . ")";
    }
}

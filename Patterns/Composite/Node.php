<?php

namespace Patterns\Composite;

/**
 * Класс Узел представляет собой конечные объекты структуры. Не может иметь
 * вложенных компонентов.
 */
class Node extends Component
{
    public function operation(): string
    {
        return "Узел";
    }
}

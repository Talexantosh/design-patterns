<?php

namespace Patterns\Composite;

/**
 * Клиентский код работает со всеми компонентами через базовый интерфейс.
 */
function clientCode(Component $component)
{
    // ...

    echo "РЕЗУЛЬТАТ: " . $component->operation();

    // ...
}

$simple = new Node();
echo "Клиент: у меня есть простой компонент:<br/>";
clientCode($simple);
echo "<br/><br/>";

/**
 * ...а также сложные контейнеры.
 */
$tree = new Container();
$branch1 = new Container();
$branch1->addChild(new Node());
$branch1->addChild(new Node());
$branch2 = new Container();
$branch2->addChild(new Node());
$tree->addChild($branch1);
$tree->addChild($branch2);
echo "Клиент: Теперь у меня есть дерево контейнеров:<br/>";
clientCode($tree);
echo "<br/><br/>";

function clientCode2(Component $component1, Component $component2)
{
    // ...

    if ($component1->isComposite()) {
        $component1->addChild($component2);
    }
    echo "РЕЗУЛЬТАТ: " . $component1->operation();

    // ...
}

echo "Клиент: не нужно проверять классы компонентов даже при управлении деревом:<br/>";
clientCode2($tree, $simple);
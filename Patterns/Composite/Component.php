<?php

namespace Patterns\Composite;

/**
 * Базовый класс Компонент объявляет общие операции как для простых, так и для
 * сложных объектов структуры.
 */
abstract class Component
{
    /**
     * @var Component
     */
    protected Component $parent;

    public function setParent(Component $parent): void
    {
        $this->parent = $parent;
    }

    public function getParent(): Component
    {
        return $this->parent;
    }

    /**
     * не нужно будет предоставлять конкретные классы компонентов клиентскому коду. 
     * Но методы будут пустыми для компонентов уровня узла.
     */
    public function addChild(Component $component): void { }

    public function removeChild(Component $component): void { }

    /**
     * позволит клиентскому коду понять,
     * может ли компонент иметь вложенные объекты.
     */
    public function isComposite(): bool
    {
        return false;
    }

    /**
     * некоторое поведение по умолчанию.
     */
    abstract public function operation(): string;
}

<?php
declare(strict_types=1); 

    namespace Patterns\FactoryMethod;

    /**
     * Паттерн Фабричный метод применим тогда, когда в 
     * программе есть иерархия классов продуктов.
     */
    interface Button{
        /**
         * Отрисовать кнопку
         */
        public function render(): string;
    }
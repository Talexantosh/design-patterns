<?php
declare(strict_types=1);

    namespace Patterns\FactoryMethod\Button;

    use Patterns\FactoryMethod\Button as Button;

    class LinuxButton implements Button{
        /**
         * @inheritdoc
         */
        public function render(): string {
            return '<button>Download for Linux</button>';
        }
    }
<?php
declare(strict_types=1);

    namespace Patterns\FactoryMethod\Button;

    use Patterns\FactoryMethod\Button as Button;

    class WindowsButton implements Button{
        /**
         * @inheritdoc
         */
        public function render(): string{
            return '<button>Download for Windows</button>';
        }
    }
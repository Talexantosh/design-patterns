<?php
declare(strict_types=1);

    namespace Patterns\FactoryMethod\Loader;

    use Patterns\FactoryMethod\Loader as Loader;
    use Patterns\FactoryMethod\Button\LinuxButton as LinuxButton;

    /**
     * Загрузчик для Linux
     */
    class LinuxLoader extends Loader{
        /**
         * @inheritdoc
         */
        public function createButton(){
            $this->button = new LinuxButton();
            return $this->button;
        }
        /**
         * Конкретные фабрики переопределяют фабричный метод и
         * возвращают из него собственные продукты.
         */
    }
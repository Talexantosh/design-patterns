<?php
declare(strict_types=1);

    namespace Patterns\FactoryMethod\Loader;

    use Patterns\FactoryMethod\Loader as Loader;
    use Patterns\FactoryMethod\Button\WindowsButton as WindowsButton;

    /**
     * Загрузчик для Windows
     */
    class WindowsLoader extends Loader{
        /**
         * @inheritdoc
         */
        public function createButton(){
            $this->button = new WindowsButton();
            return $this->button;
        }
    }
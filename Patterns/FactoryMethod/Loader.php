<?php
    namespace Patterns\FactoryMethod;

    use Patterns\FactoryMethod\Button as Button;

    /**
     * Мы выносим весь код создания продуктов в особый метод,
     * который назвают "фабричным".
     */
    abstract class Loader{

        /**
         * Объект кнопка
         * @var Button $button
         */
        protected Button $button;

        /**
         * Отрисовать кнопку загрузки
         */
        public function render():void{
            if (isset($this->button)){
                echo($this->button->render());
            }
        }

        /**
         * Фабричный метод:
         * Создать кнопку загрузки
         * @return Button
         */
        abstract public function createButton();
    }
<?php
declare(strict_types=1);

namespace Patterns\Mediator\UIComponent;

abstract class Component {
// Классы компонентов общаются с посредниками через их общий
// интерфейс. Благодаря этому одни и те же компоненты можно
// использовать в разных посредниках.

/**
 * @var MediatorInterface
 */
protected \Patterns\Mediator\MediatorInterface $dialog;

public function __construct(\Patterns\Mediator\MediatorInterface $dialog) {
    $this->dialog = $dialog;
}

/**
 * @param void
 * @return void
 */
abstract public function send():void;

}
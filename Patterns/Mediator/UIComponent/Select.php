<?php
declare(strict_types=1);

namespace Patterns\Mediator\UIComponent;

// Конкретные компоненты не связаны между собой напрямую. У них
// есть только один канал общения — через отправку уведомлений
// посреднику.
class Select extends Component {

    /**
     * @inheritdoc
     */
    public function  send():void {
        $this->dialog->notify($this, "You selected somethings");
    }
}
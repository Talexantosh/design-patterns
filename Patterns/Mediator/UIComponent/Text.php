<?php
declare(strict_types=1);

namespace Patterns\Mediator\UIComponent;

// Конкретные компоненты не связаны между собой напрямую. У них
// есть только один канал общения — через отправку уведомлений
// посреднику.
class Text extends Component {
    public string $buffer = "";

    public function  send():void {
    }
}
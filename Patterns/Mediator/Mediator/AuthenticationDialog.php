<?php
declare(strict_types=1);

namespace Patterns\Mediator\Mediator;

use Patterns\Mediator\MediatorInterface as Mediator;
use Patterns\Mediator\UIComponent\Select;
use Patterns\Mediator\UIComponent\Text;
use Patterns\Mediator\UIComponent\Button;

// Конкретный посредник. Все связи между конкретными
// компонентами переехали в код посредника. Он получает
// извещения от своих компонентов и знает, как на них
// реагировать.
class AuthenticationDialog implements Mediator {

    /**
     * @var Select
     */
    private Select $select;

    /**
     * @var Text
     */
    private Text $text;

    /**
     * @var Button
     */
    private Button $okBtn;

    public function __construct() {
        // Здесь нужно создать объекты всех компонентов, подав
        // текущий объект-посредник в их конструктор.
        $this->select = new Select($this);
        $this->text = new Text($this);
        $this->okBtn = new Button($this);
    }

    // Когда что-то случается с компонентом, он шлёт посреднику
    // оповещение. После получения извещения посредник может
    // либо сделать что-то самостоятельно, либо перенаправить
    // запрос другому компоненту.
    public function notify(mixed $sender, mixed $event):void {
        if ($sender == $this->select)
            $this->text->buffer = $event;

        if ($sender == $this->okBtn)
            $this->text->buffer = "Data recieved";
            
        }
    }

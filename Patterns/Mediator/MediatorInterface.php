<?php
declare(strict_types=1);

namespace Patterns\Mediator;

// Общий интерфейс посредников.
interface Mediator {

    /**
     * @param Component
     * @param string
     */
    public function notify(\Patterns\Mediator\UIComponent\Component $sender, string $event);
}

<?php
declare(strict_types=1);
namespace Controller\Sql;

use Controller\AbstractController;
use Model\Pdo\Subquery as Model;
use View\ViewDashboard;

class Subquery extends AbstractController
{
    public function __construct()
    {
        $this->model = new Model();
        $this->view = new ViewDashboard($this->model);
    }

    public function execute(): void
    {
        $this->view->setSectionTitle("Корреляционный подзапрос.");
        $this->view->output("index.phtml");
    }
}
<?php
declare(strict_types=1);
namespace Controller\Sql;

use Controller\AbstractController;
use Model\Pdo\Client;
use View\ViewDashboard;

class TopClient extends AbstractController
{
    public function __construct()
    {
        $this->model = new Client();
        $this->view = new ViewDashboard($this->model);
    }

    public function execute(): void
    {
        $this->view->setSectionTitle("Клиент который купил максимальное количество товаров.");
        $this->view->output("index.phtml");
    }
}
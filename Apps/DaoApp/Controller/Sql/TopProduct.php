<?php
declare(strict_types=1);
namespace Controller\Sql;

use Controller\AbstractController;
use Model\Pdo\Product;
use View\ViewDashboard;

class TopProduct extends AbstractController
{
    public function __construct()
    {
        $this->model = new Product();
        $this->view = new ViewDashboard($this->model);
    }

    public function execute(): void
    {
        $this->view->setSectionTitle("Топ 10 самых дорогих товаров.");
        $this->view->output("index.phtml");
    }
}
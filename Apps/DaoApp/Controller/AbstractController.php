<?php
declare(strict_types=1);

namespace Controller;

use Model\DatabaseInterface;
use View\AbstractView;

abstract class AbstractController
{
    protected DatabaseInterface $model;

    protected AbstractView $view;

    abstract public function execute(): void;
}
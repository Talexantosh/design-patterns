<?php
declare(strict_types=1);
namespace Controller\Laravel;

use Controller\AbstractController;
use Model\Repository\SubqueryRepository as Collection;
use DataProvider\DataProvider;
use View\ViewDashboard;

class Subquery extends AbstractController
{
    public function __construct()
    {
        $this->model = new DataProvider();
        $this->view = new ViewDashboard($this->model);
    }

    public function execute(): void
    {        
        $collection = new Collection();

        $this->model->setData($collection->getList());

        $this->view->setSectionTitle("Правое соединение таблиц.");
        $this->view->output("index.phtml");
    }
}
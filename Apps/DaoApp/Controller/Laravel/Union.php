<?php
declare(strict_types=1);
namespace Controller\Laravel;

use Controller\AbstractController;
use Model\Repository\UnionRepository as Collection;
use DataProvider\DataProvider;
use View\ViewDashboard;

class Union extends AbstractController
{
    public function __construct()
    {
        $this->model = new DataProvider();
        $this->view = new ViewDashboard($this->model);
    }

    public function execute(): void
    { 
        $collection = new Collection();
        
        $this->model->setData($collection->getList());

        $this->view->setSectionTitle("Объединение таблиц.");
        $this->view->output("index.phtml");
    }
}
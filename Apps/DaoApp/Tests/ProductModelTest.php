<?php
declare(strict_types=1);

namespace Tests;

use Model\Pdo\Product as SqlModel;
use Model\Repository\ProductRepository as LaravelModel;
use PHPUnit\Framework\TestCase;

class ProductModelTest extends TestCase
{
    static array $testSqlModel; 

    static array $testLaravelModel; 

    public static function setUpBeforeClass(): void
    {
        self::$testSqlModel = (new SqlModel())->getResult();
        self::$testLaravelModel = (new LaravelModel())->getList();
    }

    public function testGetResultEquals(): void
    {
        $this->assertEquals(self::$testSqlModel[0]['id_product'], self::$testLaravelModel[0]['id_product']);
        $this->assertEquals(self::$testSqlModel[0]['id_category'], self::$testLaravelModel[0]['id_category']);
        $this->assertEquals(self::$testSqlModel[0]['name_product'], self::$testLaravelModel[0]['name_product']);
        $this->assertEquals(self::$testSqlModel[0]['price'], self::$testLaravelModel[0]['price']);
    }

    public function testGetResultCount(): void
    {
        $this->assertCount(10, self::$testLaravelModel);
        $this->assertCount(10, self::$testSqlModel);
    }
}
<?php
declare(strict_types=1);

namespace Tests;

use DataProvider\DataProvider;
use Illuminate\Database\Eloquent\Collection;
use Model\LaravelOrm\ShopCart;
use PHPUnit\Framework\TestCase;
use Model\Repository\SubqueryRepository as Model;
use View\ViewDashboard;

class SubqueryTest extends TestCase
{
    public function testCollectionDataProvider(): array
    {
        $shopCart = $this->getMockBuilder(ShopCart::class)
        ->setMethods(['get'])
        ->getMock();

        $collection = $this->getMockBuilder(Collection::class)
        ->setMethods(['toArray'])
        ->getMock();
        $collection->expects($this->any())
        ->method('toArray')
        ->will($this->returnValue([
                                    '0' => [
                                    'id_order' => 1,
                                    'id_product' => 1,
                                    'product' => 'Name Product',
                                    'price' => 20.21
                                            ]
                                    ]));

        $shopCart->expects($this->any())
        ->method('get')
        ->will($this->returnValue($collection));
        
        
        $data = $shopCart->get();

        $this->assertIsArray($data->toArray());
    
        return $data->toArray();
    }

    /**
     * @depends testCollectionDataProvider
     */
    public function testSubqueryExecute(array $collection)
    {
        $repo = $this->getMockBuilder(Model::class)
                        ->setMethods(['getList'])
                        ->getMock();
        $repo->expects($this->any())
             ->method('getList')
             ->will($this->returnValue($collection));

        $provider = new DataProvider();
        
        $provider->setData($repo->getList());

        $this->assertIsArray($provider->getResult());
        $this->assertArrayHasKey('price', $provider->getResult()[0]);

        $view = new ViewDashboard($provider);

        $view->setSectionTitle("Правое соединение таблиц.");

        $this->assertEquals("Правое соединение таблиц.", $view->sectionTitle());
    }
}
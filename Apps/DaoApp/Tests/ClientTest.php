<?php
declare(strict_types=1);

namespace Tests;

use Model\Pdo\Client as SqlModel;
use Model\Repository\ClientRepository as LaravelModel;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    public function testGetResultHasKey(): void
    {
        $testSqlModel = (new SqlModel())->getResult();
        $testLaravelModel = (new LaravelModel())->getList();

        $this->assertArrayHasKey('max_qty', $testLaravelModel[0]);
        $this->assertArrayHasKey('max_qty', $testSqlModel[0]);
        $this->assertArrayHasKey('id_user', $testLaravelModel[0]);
        $this->assertArrayHasKey('id_user', $testSqlModel[0]);
        $this->assertArrayHasKey('fname', $testLaravelModel[0]);
        $this->assertArrayHasKey('fname', $testSqlModel[0]);
        $this->assertArrayHasKey('lname', $testLaravelModel[0]);
        $this->assertArrayHasKey('lname', $testSqlModel[0]);
        $this->assertArrayHasKey('email', $testLaravelModel[0]);
        $this->assertArrayHasKey('email', $testSqlModel[0]);
    }
}
<?php
declare(strict_types=1);

namespace Tests;

use DataProvider\DataProvider;
use Model\LaravelOrm\Join;
use PHPUnit\Framework\TestCase;
use Model\Repository\JoinRepository;
use View\ViewDashboard;

class JoinTest extends TestCase
{
    public function testJoinRepository(): array
    {
        $repo = new JoinRepository();

        $collection = $repo->getList();

        $this->assertIsArray($collection);

        return [$collection];
    }

    /**
     * @dataProvider testJoinRepository
     */
    public function testJoinController($collection): void
    {
        $model = new DataProvider();
        $view = new ViewDashboard($model);

        $model->setData($collection);
        $this->assertEquals($collection, $model->getResult());

        $view->setSectionTitle("Правое соединение таблиц.");
        $this->assertEquals("Правое соединение таблиц.", $view->sectionTitle());
    }

    
    /**
     * @expectedException BadMethodCallException
     * @dataProvider testJoinRepository
     */
    public function testModelException($collection): void
    {
        $model = new Join();
        $this->expectException(\BadMethodCallException::class);
        $model->setData($collection);
    }
}
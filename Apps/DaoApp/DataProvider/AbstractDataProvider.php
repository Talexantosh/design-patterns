<?php

declare(strict_types=1);

namespace DataProvider;

use Model\DatabaseInterface;

abstract class AbstractDataProvider implements DatabaseInterface
{
    private array $data;

    public function setData(array $data)
    {
        $this->data = $data;
    }

    public function getResult(): array
    {
        return $this->data;
    }

    public function resultKey(): array
    {
        return array_keys($this->data[0]);
    }
}
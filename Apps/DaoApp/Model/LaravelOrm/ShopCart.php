<?php
declare(strict_types=1);

namespace Model\LaravelOrm;

use \Model\LaravelOrmModel;

class ShopCart extends LaravelOrmModel
{
    protected $table = 'shopping_cart';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
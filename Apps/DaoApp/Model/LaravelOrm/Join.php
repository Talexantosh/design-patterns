<?php
declare(strict_types=1);

namespace Model\LaravelOrm;

use \Model\LaravelOrmModel;

class Join extends LaravelOrmModel
{
    protected $table = 'shopping_order';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id_order';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
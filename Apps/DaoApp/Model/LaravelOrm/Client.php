<?php
declare(strict_types=1);

namespace Model\LaravelOrm;

use \Model\LaravelOrmModel;

class Client extends LaravelOrmModel
{
    protected $table = 'user';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id_user';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
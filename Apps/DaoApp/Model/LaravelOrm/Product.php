<?php
declare(strict_types=1);

namespace Model\LaravelOrm;

use \Model\LaravelOrmModel;

class Product extends LaravelOrmModel
{
    protected $table = 'product';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id_product';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
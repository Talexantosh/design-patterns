<?php

declare(strict_types=1);

namespace Model\Repository;

use Model\LaravelOrm\ShopCart;
use Model\LaravelOrm\Order;
use Model\LaravelOrm\Client;

class ClientRepository extends AbstractRepository 
{
   public function getList(): array
   { 
        $shopCart = new ShopCart();
        $order = new Order();
        $client = new Client();

        $subquery1 = $shopCart->select($shopCart->raw('sum(qty) as max_qty, id_order'))->groupBy('id_order')->orderBy('max_qty', 'desc')->limit(1);
        
        $subquery2 = $order->joinSub($subquery1, 't', function($join){
            $join->on('t.id_order', '=', $join->raw('shopping_order.id_order'));
        })->select('t.max_qty', $order::raw('shopping_order.id_order, shopping_order.id_user'));

        $data = $client->joinSub($subquery2, 't', function($join){
            $join->on('t.id_user', '=', 'user.id_user');
        })->select('t.max_qty', $client->raw('user.*'))->get();

        return $data->toArray();
    }
}
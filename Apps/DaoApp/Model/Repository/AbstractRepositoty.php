<?php

declare(strict_types=1);

namespace Model\Repository;

abstract class AbstractRepository 
{
   public function getById(int $id): array
   { return [];}

   public function save(array $data): void {}

   public function getList(): array
   { return [];}
}
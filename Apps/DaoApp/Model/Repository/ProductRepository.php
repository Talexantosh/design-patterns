<?php

declare(strict_types=1);

namespace Model\Repository;

use Model\LaravelOrm\Product as Model;

class ProductRepository extends AbstractRepository 
{
   public function getList(): array
   { 
        $model = new Model();
        $data = $model->orderByDesc('price')
                    ->limit(10)
                    ->get();

        return $data->toArray();
    }
}
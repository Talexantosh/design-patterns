<?php

declare(strict_types=1);

namespace Model\Repository;

use Model\LaravelOrm\Join as Model;

class JoinRepository extends AbstractRepository 
{
   public function getList(): array
   { 
        $model = new Model();
        $data = $model->rightJoin('shopping_cart', function ($join) use($model){
                    $join->on('shopping_cart.id_order', '=', $model->raw('2'));})
                    ->get();

        return $data->toArray();
    }
}
<?php

declare(strict_types=1);

namespace Model\Repository;

use Model\LaravelOrm\ShopCart;

class SubqueryRepository extends AbstractRepository 
{
   public function getList(): array
   { 
    $shopCart = new ShopCart();

    $data = $shopCart->from('shopping_cart as tb1')
                    ->select('id_order', 'id_product')
                    ->selectSub(function($query){
                        $query->from('product as tb2')
                        ->selectRaw('name_product')
                        ->whereRaw('tb1.id_product=tb2.id_product');
                    }, 'product')
                    ->selectSub(function($query){
                        $query->from('product as tb3')
                        ->selectRaw('price')
                        ->whereRaw('tb1.id_product=tb3.id_product');
                    }, 'price')->get();

    return $data->toArray();
}
}
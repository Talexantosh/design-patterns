<?php

declare(strict_types=1);

namespace Model\Repository;

use Model\LaravelOrm\Union as Model;
use Model\LaravelOrm\Product as ProductModel;

class UnionRepository extends AbstractRepository 
{
   public function getList(): array
   { 
        $model = new Model();
        $union = new ProductModel();
        $data = $model->union($union)->get();
        return $data->toArray();
    }
}
<?php
declare(strict_types=1);

namespace Model\Pdo;

use Model\PdoModel;

class Join extends PdoModel
{
    private array $data;

    public function init(): void
    {
        $sth = $this->prepare("select tb1.*, tb2.* from shopping_order as tb1
        right join shopping_cart as tb2
        on tb2.id_order=2;");
        $sth->execute();
        $this->data = $sth->fetchAll();
    }

    public function getResult(): array
    {
        return $this->data;
    }

    public function resultKey(): array
    {
        return array_keys($this->data[0]);
    }
}
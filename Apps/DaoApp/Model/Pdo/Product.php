<?php
declare(strict_types=1);

namespace Model\Pdo;

use Model\PdoModel;

class Product extends PdoModel
{
    private array $data;

    public function init(): void
    {
        $sth = $this->prepare("select * from product order by price desc limit 10;");
        $sth->execute();
        $this->data = $sth->fetchAll();
    }

    public function getResult(): array
    {
        return $this->data;
    }

    public function resultKey(): array
    {
        return array_keys($this->data[0]);
    }
}
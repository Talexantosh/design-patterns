<?php
declare(strict_types=1);

namespace Model\Pdo;

use Model\PdoModel;

class Subquery extends PdoModel
{
    private array $data;

    public function init(): void
    {        
        $sth = $this->prepare("select id_order, id_product,
        (select name_product from product tb2
            where tb1.id_product=tb2.id_product) as product,
        (select price from product tb3
            where tb1.id_product=tb3.id_product) as price
        from shopping_cart as tb1;");
        $sth->execute();
        $this->data = $sth->fetchAll();
    }

    public function getResult(): array
    {
        return $this->data;
    }

    public function resultKey(): array
    {
        return array_keys($this->data[0]);
    }
}
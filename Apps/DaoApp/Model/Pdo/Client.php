<?php
declare(strict_types=1);

namespace Model\Pdo;

use Model\PdoModel;

class Client extends PdoModel
{
    private array $data;

    public function init(): void
    {
        $sth = $this->prepare("create temporary table price select t.id_user, t.id_order, t.total_price, 
        shopping_cart.id_product, shopping_cart.subtotal_price, shopping_cart.qty
        from  shopping_cart
        join (select user.id_user, shopping_order.id_order, shopping_order.total_price 
                from user
                join shopping_order on user.id_user=shopping_order.id_user) as t
        on t.id_order=shopping_cart.id_order;");
        $sth->execute();
        
        $sth = $this->prepare("select max(tb1.sum_qty) as max_qty, tb2.* 
        from (select sum(qty) as sum_qty, id_user from price group by id_user) as tb1
        join user as tb2
        on tb1.id_user=tb2.id_user;");
        $sth->execute();
        $this->data = $sth->fetchAll();
    }

    public function getResult(): array
    {
        return $this->data;
    }

    public function resultKey(): array
    {
        return array_keys($this->data[0]);
    }
}
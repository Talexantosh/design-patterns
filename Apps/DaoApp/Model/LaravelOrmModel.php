<?php
declare(strict_types=1);

namespace Model;

use Model\DatabaseInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as Capsule;

class LaravelOrmModel extends Model implements DatabaseInterface
{
    protected static ?Capsule $capsule = null;

    public function __construct(array $attributes = ['driver' => 'mysql', 
                                'host' => '127.0.0.1', 
                                'database' => 'am_learn', 
                                'username' => 'root', 
                                'password' => ''])
    {
        {
            // The database manager instance
            static::$capsule = new Capsule();

            // Register a connection with the manager
            static::$capsule->addConnection($attributes);

            // Make this capsule instance available globally
            static::$capsule->setAsGlobal();

            // Bootstrap Eloquent so it is ready for usage
            static::$capsule->bootEloquent();
        }
    }
}

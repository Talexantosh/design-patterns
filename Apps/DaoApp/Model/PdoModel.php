<?php
declare(strict_types=1);

namespace Model;

use Model\DatabaseInterface;

class PdoModel extends \PDO implements DatabaseInterface
{
    private array $context = [];

    public function __construct(string $driver = 'mysql', 
                                string $host = '127.0.0.1', 
                                string $database = 'am_learn', 
                                string $username = 'root', 
                                string $password = '')
    {
        $dsn = "{$driver}:dbname={$database};host={$host}";

        try
        {
            parent::__construct($dsn, $username, $password);
        }
        catch(\PDOException $e)
        {
            echo $e;
        }

        $this->init();
    }

    public function setContext(array $context = []): DatabaseInterface
    {
        $this->context = $context;

        return $this;
    }

    public function getConnection(): DatabaseInterface
    {
        return $this;
    }

    public function init(): void {}
}
<?php
declare(strict_types=1);

namespace View;

class ViewDashboard extends AbstractView
{
    protected string $title;
    
    public function setSectionTitle(string $title): void
    {
        $this->title = $title;
    }  
    
    public function sectionTitle(): string
    {
        return $this->title;
    }

}
<?php
declare(strict_types=1);

namespace View;

use Model\DatabaseInterface;

abstract class AbstractView
{
    protected DatabaseInterface $model;

    public function __construct(DatabaseInterface $model)
    {
        $this->model = $model;
    }

    public function output(string $template): void
    {
        $view = $this;

        require_once(__DIR__ . '/web/'. $template);
    }

    public function getModel(): DatabaseInterface
    {
        return $this->model;
    }
}
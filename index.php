<?php
    function  _autoload($pClassName){
        include(__DIR__.'\\'.$pClassName.".php");
    }

    spl_autoload_register("_autoload");

 //   require 'Patterns\Singleton\test.php';
 //   require 'Patterns\FactoryMethod\test.php';
 //   require 'Patterns\AbstractFactory\test.php';
 //   require 'Patterns\Builder\test.php';
 //   require 'Patterns\Prototype\test.php';
 //   require 'Patterns\Command\test.php';
 //   require 'Patterns\Iterator\test.php';
 //  require 'Patterns\Facade\test.php';
   require 'Patterns\Strategy\test.php';
 //  require 'Patterns\Memento\test.php';
 //   require 'Patterns\TemplateMethod\test.php';
 //  require 'Patterns\State\test.php';
 //   require 'Patterns\Visitor\test.php';
 //   require 'Patterns\Composite\test.php';
 //   require 'Patterns\MVC\test.php';
